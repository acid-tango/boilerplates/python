#!/usr/bin/env python3
"""
Python Boilerplate
"""
from my_function import my_function


def main():
    """
    Main function
    """
    print(my_function())


if __name__ == "__main__":
    main()
